var request = require('supertest');
var server = require('./server');

describe('GET /', function() {
    it('respond with hello world', function(done) {
        request(server.app)
            .get('/')
            .expect('Hello World!')
            .expect(200, done);
    });
});
