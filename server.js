var express = require('express');

var app = express();
exports.app = app;

app.get('/', function (req, res) {
    res.send('Hello World!');
});

if (require.main === module) {
    app.listen(3000, function () {
        console.log('Example app listening on port 3000!');
    });
}
